-- Lua Dissector for HKEP
-- Author: Ryosuke Yamamoto
--
-- to use in Wireshark:
-- 1) Ensure your Wireshark works with Lua plugins - "About Wireshark" should say it is compiled with Lua
-- 2) Install this dissector in the proper plugin directory - see "About Wireshark/Folders" to see Personal
--    and Global plugin directories.  After putting this dissector in the proper folder, "About Wireshark/Plugins"
--    should list "HKEP.lua"
-- 3) Capture packets of HKEP
-- 4) Tie the port number to "HKEP" with "Decode As..."
-- 5) You will now see the HKEP Data dissection of the TCP/IP Packet
-- 6) Filtering by TCP port number will make it easier to see
--
-- * If the length of the HKEP message is inappropriate, an Error may be displayed.
--
-- This program is free software.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY. without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
--
--
------------------------------------------------------------------------------------------------

-- プロトコルの定義
hkep_proto = Proto("hkep","HKEP","HKEP protocol")
local massagetype = ""

-- 表示
function diso_hkep(buffer,pinfo,tree)
	pinfo.cols.protocol = "HKEP"
    local msg_size = buffer(0, 2):uint()
    local msg_id = buffer(2, 1):uint()
    local msg_type = "AKE_PreInit"

	if msg_id == 1 then
		massagetype = massagetype .. "[Null message]"
		pinfo.cols.info = "MassageType: Null message"
		local hkeptree = tree:add(hkep_proto,buffer(),"HKEP Protocol, Message: Null message" )
		hkeptree:add(buffer(0,2),"msg_size: " .. msg_size )
		hkeptree:add(buffer(2,1),"msg_id: " .. msg_id .. "  MassageType: Null message" )

	elseif msg_id == 2 then
		massagetype = massagetype .. "[AKE_Init] Encoder->Decoder"
		local hkeptree = tree:add(hkep_proto,buffer(),"HKEP Protocol, Message: AKE_Init")
		hkeptree:add(buffer(0,2),"msg_size: " .. msg_size )
		hkeptree:add(buffer(2,1),"msg_id: " .. msg_id .. "  MassageType: AKE_Init" )
		hkeptree:add(buffer(3,8),"r tx[63:0]: " .. buffer(3,8) )

	elseif msg_id == 3 then
		massagetype = massagetype .. "[AKE_Send_Cert] Decoder->Encoder"
		local hkeptree = tree:add(hkep_proto,buffer(),"HKEP Protocol, Message: AKE_Send_Cert" ) 
		hkeptree:add(buffer(0,2),"msg_size: " .. msg_size )
		hkeptree:add(buffer(2,1),"msg_id: " .. msg_id .. "  MassageType: AKE_Send_Cert" )
		if buffer(3,1):uint() == 1 then
			hkeptree:add(buffer(3,1),"REPEATER: true"  )
		else
			hkeptree:add(buffer(3,1),"REPEATER: false" )
		end
		local protocol_descriptor = buffer(140,2):uint()
		local sertrx_reserved = protocol_descriptor % 4096
		protocol_descriptor = ( protocol_descriptor - sertrx_reserved ) / 4096
		local sertrx_subtree = hkeptree:add(buffer(4,522),"cert rx[4175:0]: " .. buffer(4,522) )
		sertrx_subtree:add(buffer(4,5),"Receiver ID: " .. buffer(4,5) .." (cert rx[4175:4136])")
		sertrx_subtree:add(buffer(9,131),"Receiver Public Key: " .. buffer(9,131) .." (cert rx[4135:3088])")
		sertrx_subtree:add(buffer(140,1),"Protocol Descriptor: " .. protocol_descriptor .." (cert rx[3087:3084])")
		sertrx_subtree:add(buffer(140,2),"Reserved: " .. sertrx_reserved .." (cert rx[3083:3072])")
		sertrx_subtree:add(buffer(142,384),"DCP LLC Signature: " .. buffer(142,384) .." (cert rx[3071:0])")

	elseif msg_id == 4 then
		massagetype = massagetype .. "[AKE_No_Stored_km] Encoder->Decoder"
		local hkeptree = tree:add(hkep_proto,buffer(),"HKEP Protocol, Message: AKE_No_Stored_km" )
		hkeptree:add(buffer(0,2),"msg_size: " .. msg_size )
		hkeptree:add(buffer(2,1),"msg_id: " .. msg_id .. "  MassageType: AKE_No_Stored_km" )
		hkeptree:add(buffer(3,128),"Ekpub_km[1023:0]: " .. buffer(3,128) )

	elseif msg_id == 5 then
		massagetype = massagetype .. "[AKE_Stored_km] Encoder->Decoder"
		local hkeptree = tree:add(hkep_proto,buffer(),"HKEP Protocol, Message: AKE_Stored_km" )
		hkeptree:add(buffer(0,2),"msg_size: " .. msg_size )
		hkeptree:add(buffer(2,1),"msg_id: " .. msg_id .. "  MassageType: AKE_Stored_km" )
		hkeptree:add(buffer(3,16),"Ekh_km[127:0]: " .. buffer(3,16) )
		hkeptree:add(buffer(19,16),"m[127:0]: " .. buffer(19,16) )

	elseif msg_id == 6 then
		massagetype = massagetype .. "[AKE_Send_rrx] Decoder->Encoder"
		local hkeptree = tree:add(hkep_proto,buffer(),"HKEP Protocol, Message: AKE_Send_rrx" )
		hkeptree:add(buffer(0,2),"msg_size: " .. msg_size )
		hkeptree:add(buffer(2,1),"msg_id: " .. msg_id .. "  MassageType: AKE_Send_rrx" )
		hkeptree:add(buffer(3,8),"r rx[63:0]: " .. buffer(3,8) )

	elseif msg_id == 7 then
		massagetype = massagetype .. "[AKE_Send_H_prime] Decoder->Encoder"
		local hkeptree = tree:add(hkep_proto,buffer(),"HKEP Protocol, Message: AKE_Send_H_prime" )
		hkeptree:add(buffer(0,2),"msg_size: " .. msg_size )
		hkeptree:add(buffer(2,1),"msg_id: " .. msg_id .. "  MassageType: AKE_Send_H_prime" )
		hkeptree:add(buffer(3,32),"H'[255:0]: " .. buffer(3,32) )

	elseif msg_id == 8 then
		massagetype = massagetype .. "[AKE_Send_Pairing_Info] Decoder->Encoder"
		local hkeptree = tree:add(hkep_proto,buffer(),"HKEP Protocol, Message: AKE_Send_Pairing_Info" )
		hkeptree:add(buffer(0,2),"msg_size: " .. msg_size )
		hkeptree:add(buffer(2,1),"msg_id: " .. msg_id .. "  MassageType: AKE_Send_Pairing_Info" )
		hkeptree:add(buffer(3,16),"Ekh_km[127:0]: " .. buffer(3,16) )

	elseif msg_id == 9 then
		massagetype = massagetype .. "[LC_Init] Encoder->Decoder"
		local hkeptree = tree:add(hkep_proto,buffer(),"HKEP Protocol, Message: LC_Init" )
		hkeptree:add(buffer(0,2),"msg_size: " .. msg_size )
		hkeptree:add(buffer(2,1),"msg_id: " .. msg_id .. "  MassageType: LC_Init" )
		hkeptree:add(buffer(3,8),"r n[63:0]: " .. buffer(3,8) )

	elseif msg_id == 10 then
		massagetype = massagetype .. "[LC_Send_L_prime] Decoder->Encoder"
		local hkeptree = tree:add(hkep_proto,buffer(),"HKEP Protocol, Message: LC_Send_L_prime" )
		hkeptree:add(buffer(0,2),"msg_size: " .. msg_size )
		hkeptree:add(buffer(2,1),"msg_id: " .. msg_id .. "  MassageType: LC_Send_L_prime" )
		if msg_size == 35 then
			hkeptree:add(buffer(3,32),"L'[255:0]: " .. buffer(3,32) )
		else 
			hkeptree:add(buffer(3,16),"L'[255:128]: " .. buffer(3,16) )
		end

	elseif msg_id == 11 then
		massagetype = massagetype .. "[SKE_Send_Eks] Encoder->Decoder"
		local hkeptree = tree:add(hkep_proto,buffer(),"HKEP Protocol, Message: SKE_Send_Eks" )
		hkeptree:add(buffer(0,2),"msg_size: " .. msg_size )
		hkeptree:add(buffer(2,1),"msg_id: " .. msg_id .. "  MassageType: SKE_Send_Eks" )
		hkeptree:add(buffer(3,16),"Edkey_Ks[127:0]: " .. buffer(3,16) )
		hkeptree:add(buffer(19,8),"r iv[63:0]: " .. buffer(19,8) )
		if msg_size == 59 then
			hkeptree:add(buffer(27,32),"HMAC(r iv)[255:0]: " .. buffer(27,32) )
			hkeptree:add(buffer(27,32)," (The receiver complies with HDCP2.3 or higher.)" )
		else
			hkeptree:add(buffer(0,27)," (The receiver is not compliant with HDCP2.3 or above.)" )
		end

	elseif msg_id == 12 then
		massagetype = massagetype .. "[RepeaterAuth_Send_ReceiverID_List] Decoder->Encoder"
		local hkeptree = tree:add(hkep_proto,buffer(),"HKEP Protocol, Message: RepeaterAuth_Send_ReceiverID_List" )
		hkeptree:add(buffer(0,2),"msg_size: " .. msg_size )
	    local max_devs_exceeded = buffer(3,1):uint()
	    local max_cascade_exceeded = buffer(4,1):uint()
		local repeaterauth_send_receiverid_list_type = ( msg_size - 39 ) % 5
		hkeptree:add(buffer(2,1),"msg_id: " .. msg_id .. "  MassageType: RepeaterAuth_Send_ReceiverID_List" )
		if repeaterauth_send_receiverid_list_type == 0 then
			hkeptree:add("  << The upstream HDCP Transmitter is an HDCP 2.0-compliant Device >>" )
		else
			hkeptree:add("  << The upstream HDCP Transmitter is not an HDCP 2.0-compliant Device >>" )
		end
		if max_devs_exceeded == 1 then
			hkeptree:add(buffer(3,1),"MAX_DEVS_EXCEEDED: true" )
		else
			hkeptree:add(buffer(3,1),"MAX_DEVS_EXCEEDED: false" )
		end
		if max_cascade_exceeded == 1 then
			hkeptree:add(buffer(4,1),"MAX_CASCADE_EXCEEDED: true" )
		else
			hkeptree:add(buffer(4,1),"MAX_CASCADE_EXCEEDED: false" )
		end
		if max_devs_exceeded ~= 1 and max_cascade_exceeded ~= 1 then
			hkeptree:add(buffer(5,1),"DEVICE_COUNT: " .. buffer(5,1):uint() )
			hkeptree:add(buffer(6,1),"DEPTH: " .. buffer(6,1):uint() )
			if repeaterauth_send_receiverid_list_type == 0 then
				hkeptree:add(buffer(7,32),"V'[255:0]: " .. buffer(7,32) )
				local j_max = ( msg_size - 39 ) / 5
				for j = 0 , j_max - 1 do
					hkeptree:add(buffer(39+(j*5),5),"Receiver_ID" .. j .. "[39:0]: " .. buffer(39+(j*5),5) )
				end
			else
				local j_max = ( msg_size - 28 ) / 5
				if buffer(7,1):uint() == 1 then
					hkeptree:add(buffer(7,1),"HDCP2_LEGACY_DEVICE_DOWNSTREAM: true" )
				else
					hkeptree:add(buffer(7,1),"HDCP2_LEGACY_DEVICE_DOWNSTREAM: false" )
				end
				if buffer(8,1):uint() == 1 then
					hkeptree:add(buffer(8,1),"HDCP1_DEVICE_DOWNSTREAM: true" )
				else
					hkeptree:add(buffer(8,1),"HDCP1_DEVICE_DOWNSTREAM: false" )
				end
				hkeptree:add(buffer(9,3),"seq_num_V: " .. buffer(9,3):uint() )
				hkeptree:add(buffer(12,16),"V'[255:128]: " .. buffer(12,16) )
				hkeptree:add("Receiver ID List" )
				for j = 0 , j_max - 1 do
					hkeptree:add(buffer(28+(j*5),5),"  Receiver_ID" .. j .. "[39:0]: " .. buffer(28+(j*5),5) )
				end
			end
		end

	elseif msg_id == 13 then
		massagetype = massagetype .. "[RTT_Ready] Decoder->Encoder"
		local hkeptree = tree:add(hkep_proto,buffer(),"HKEP Protocol, Message: RTT_Ready" )
		hkeptree:add(buffer(0,2),"msg_size: " .. msg_size )
		hkeptree:add(buffer(2,1),"msg_id: " .. msg_id .. "  MassageType: RTT_Ready" )

	elseif msg_id == 14 then
		massagetype = massagetype .. "[RTT_Challenge] Encoder->Decoder"
		local hkeptree = tree:add(hkep_proto,buffer(),"HKEP Protocol, Message: RTT_Challenge" )
		hkeptree:add(buffer(0,2),"msg_size: " .. msg_size )
		hkeptree:add(buffer(2,1),"msg_id: " .. msg_id .. "  MassageType: RTT_Challenge" )
		hkeptree:add(buffer(3,16),"L[127:0]: " .. buffer(3,16) )

	elseif msg_id == 15 then
		massagetype = massagetype .. "[RepeaterAuth_Send_Ack] Encoder->Decoder"
		local hkeptree = tree:add(hkep_proto,buffer(),"HKEP Protocol, Message: RepeaterAuth_Send_Ack" )
		hkeptree:add(buffer(0,2),"msg_size: " .. msg_size )
		hkeptree:add(buffer(2,1),"msg_id: " .. msg_id .. "  MassageType: RepeaterAuth_Send_Ack" )
		hkeptree:add(buffer(3,16),"V[127:0]: " .. buffer(3,16) )

	elseif msg_id == 16 then
		massagetype = massagetype .. "[RepeaterAuth_Stream_Manage] Encoder->Decoder"
		local hkeptree = tree:add(hkep_proto,buffer(),"HKEP Protocol, Message: RepeaterAuth_Stream_Manage" )
		hkeptree:add(buffer(0,2),"msg_size: " .. msg_size )
		local k_value = buffer(6,2):uint()
		hkeptree:add(buffer(2,1),"msg_id: " .. msg_id .. "  MassageType: RepeaterAuth_Stream_Manage" )
		hkeptree:add(buffer(3,3),"seq_num_M: " .. buffer(3,3):uint() )
		hkeptree:add(buffer(6,2),"k: " .. buffer(6,2):uint() )
		for j = 0 , k_value - 1 do
			local subtree = hkeptree:add("Stream " .. j+1,buffer(8+(j*12),12) ) 
			subtree:add(buffer(8+(j*12),4),"streamCtr: " .. buffer(8+(j*12),4) )
			subtree:add(buffer(12+(j*12),7),"ContentStreamID: " .. buffer(12+(j*12),7) )
			subtree:add(buffer(12+(j*12),4),"  Destination IP Address: IPv4: " .. buffer(12+(j*12),1):uint() .. "." .. buffer(13+(j*12),1):uint() .. "." .. buffer(14+(j*12),1):uint() .. "." .. buffer(15+(j*12),1):uint() )
			subtree:add(buffer(12+(j*12),4),"                          IPv6: ::" .. buffer(12+(j*12),2) .. ":" .. buffer(14+(j*12),2) )
			subtree:add(buffer(16+(j*12),2),"  Destination UDP Port: " .. buffer(16+(j*12),2):uint() )
			subtree:add(buffer(18+(j*12),1),"  Payload Type: " .. buffer(18+(j*12),1):uint() )
			subtree:add(buffer(19+(j*12),1),"Type: " .. buffer(19+(j*12),1):uint() )
		end

	elseif msg_id == 17 then
		massagetype = massagetype .. "[RepeaterAuth_Stream_Ready] Decoder->Encoder"
		local hkeptree = tree:add(hkep_proto,buffer(),"HKEP Protocol, Message: RepeaterAuth_Stream_Ready" )
		hkeptree:add(buffer(0,2),"msg_size: " .. msg_size )
		hkeptree:add(buffer(2,1),"msg_id: " .. msg_id .. "  MassageType: RepeaterAuth_Stream_Ready" )
		hkeptree:add(buffer(3,32),"M'[255:0]: " .. buffer(3,32) )

	elseif msg_id == 18 then
		massagetype = massagetype .. "[Receiver_AuthStatus] Decoder->Encoder"
		local hkeptree = tree:add(hkep_proto,buffer(),"HKEP Protocol, Message: Receiver_AuthStatus" )
		hkeptree:add(buffer(0,2),"msg_size: " .. msg_size )
		hkeptree:add(buffer(2,1),"msg_id: " .. msg_id .. "  MassageType: Receiver_AuthStatus" )
		hkeptree:add(buffer(3,2),"LENGTH: " .. buffer(3,2):uint() )
		if buffer(4,1):uint() == 1 then
			hkeptree:add(buffer(4,1),"REAUTH_REQ: true" )
		else
			hkeptree:add(buffer(4,1),"REAUTH_REQ: false" )
		end

	elseif msg_id == 19 then
		massagetype = massagetype .. "[AKE_Transmitter_Info] Encoder->Decoder"
		local hkeptree = tree:add(hkep_proto,buffer(),"HKEP Protocol, Message: AKE_Transmitter_Info" )
		hkeptree:add(buffer(0,2),"msg_size: " .. msg_size )
		hkeptree:add(buffer(2,1),"msg_id: " .. msg_id .. "  MassageType: AKE_Transmitter_Info" )
		hkeptree:add(buffer(3,2),"LENGTH: " .. buffer(3,2):uint() )
		hkeptree:add(buffer(5,1),"VERSION: " .. buffer(5,1):uint() )
--		hkeptree:add(buffer(6,2),"TRANSMITTER_CAPABILITY_MASK: " .. buffer(6,2) )
		local transinfo_tmp = buffer(6,2):uint()
		local transinfo_cap_msk = ( transinfo_tmp % 4 )
		local transinfo_reserved = ( transinfo_tmp - transinfo_cap_msk ) / 4
		local transinfo_locality_precompute = transinfo_cap_msk % 2
		local transinfo_ccont_category = ( transinfo_cap_msk - transinfo_locality_precompute ) / 2
		local transinfo_subtree = hkeptree:add(buffer(6,2),"TRANSMITTER_CAPABILITY_MASK: " .. buffer(6,2) )
		transinfo_subtree:add(buffer(6,2),"Reserved: " .. string.format("%x",transinfo_reserved) .. "  (TRANSMITTER_CAPABILITY_MASK[15:2])")
		if ( transinfo_ccont_category == 1 ) then
			transinfo_subtree:add(buffer(6,2),"TRANSMITTER_CONTENT_CATEGORY_SUPPORT: true  (TRANSMITTER_CAPABILITY_MASK[1])")
		else 
			transinfo_subtree:add(buffer(6,2),"TRANSMITTER_CONTENT_CATEGORY_SUPPORT: false  (TRANSMITTER_CAPABILITY_MASK[1])")
		end
		if ( transinfo_locality_precompute == 1 ) then
			transinfo_subtree:add(buffer(6,2),"TRANSMITTER_LOCALITY_PRECOMPUTE_SUPPORT: true  (TRANSMITTER_CAPABILITY_MASK[0])")
		else 
			transinfo_subtree:add(buffer(6,2),"TRANSMITTER_LOCALITY_PRECOMPUTE_SUPPORT: false  (TRANSMITTER_CAPABILITY_MASK[0])")
		end

	elseif msg_id == 20 then
		massagetype = massagetype .. "[AKE_Receiver_Info] Decoder->Encoder"
		local hkeptree = tree:add(hkep_proto,buffer(),"HKEP Protocol, Message: AKE_Receiver_Info" )
		hkeptree:add(buffer(0,2),"msg_size: " .. msg_size )
		hkeptree:add(buffer(2,1),"msg_id: " .. msg_id .. "  MassageType: AKE_Receiver_Info" )
		hkeptree:add(buffer(3,2),"LENGTH: " .. buffer(3,2):uint() )
		hkeptree:add(buffer(5,1),"VERSION: " .. buffer(5,1):uint() )
--		hkeptree:add(buffer(6,2),"RECEIVER_CAPABILITY_MASK: " .. buffer(6,2) )
		local receiverinfo_tmp = buffer(6,2):uint()
		local receiverinfo_cap_msk = ( receiverinfo_tmp % 2 )
		local receiverinfo_reserved = ( receiverinfo_tmp - receiverinfo_cap_msk ) / 2
		local receiverinfo_locality_precompute = receiverinfo_cap_msk
		local receiverinfo_subtree = hkeptree:add(buffer(6,2),"RECEIVER_CAPABILITY_MASK: " .. buffer(6,2) )
		receiverinfo_subtree:add(buffer(6,2),"Reserved: " .. string.format("%x",receiverinfo_reserved) .. "  (RECEIVER_CAPABILITY_MASK[15:1])")
		if ( receiverinfo_locality_precompute == 1 ) then
			receiverinfo_subtree:add(buffer(6,2),"RECEIVER_LOCALITY_PRECOMPUTE_SUPPORT: true  (RECEIVER_CAPABILITY_MASK[0])")
		else 
			receiverinfo_subtree:add(buffer(6,2),"RECEIVER_LOCALITY_PRECOMPUTE_SUPPORT: false  (RECEIVER_CAPABILITY_MASK[0])")
		end

	elseif msg_id == 32 then
		massagetype = massagetype .. "[AKE_PreInit] Decoder->Encoder"
		local hkeptree = tree:add(hkep_proto,buffer(),"HKEP Protocol, Message: AKE_PreInit")
		hkeptree:add(buffer(0,2),"msg_size: " .. msg_size )
	    local major_ver = buffer(3,1):uint()
	    local minor_ver = buffer(3,1):uint()
		minor_ver = minor_ver % 16 
		major_ver = ( major_ver - minor_ver ) /16
		hkeptree:add(buffer(2,1),"msg_id: " .. msg_id .. "  MassageType: AKE_PreInit" )
		hkeptree:add(buffer(3,1),"Version: " .. buffer(3,1) .. "  MajorVersion: " .. major_ver .. ", MinorVersion: " .. minor_ver )
		if buffer(4,1):uint() == 1 then
			hkeptree:add(buffer(4,1),"pairing: true" )
		else
			hkeptree:add(buffer(4,1),"pairing: false" )
		end
--		hkeptree:add(buffer(4,1),"pairing: " .. buffer(4,1))
		if buffer(5,1):uint() == 1 then
			hkeptree:add(buffer(5,1),"restart/REAUTH_REQ: true" )
		else
			hkeptree:add(buffer(5,1),"restart/REAUTH_REQ: false" )
		end
		if buffer(5,1):uint() == 1 then
			hkeptree:add(buffer(6,1),"receiver: true" )
		else
			hkeptree:add(buffer(6,1),"receiver: false" )
		end
		hkeptree:add(buffer(7,5),"receiverId: " .. buffer(7,5) )
		hkeptree:add(buffer(12,5),"portId: " .. buffer(12,5) )
		hkeptree:add(buffer(17,16),"nodeId: " .. buffer(17,16) )
		hkeptree:add(buffer(33,16),"vendorExtension: " .. buffer(33,16) )
	elseif msg_id == 33 then
		massagetype = massagetype .. "[AKE_PreInitStatus] Encoder->Decoder"
		local hkeptree = tree:add(hkep_proto,buffer(),"HKEP Protocol, Message: AKE_PreInitStatus" .. " " )
		hkeptree:add(buffer(0,2),"msg_size: " .. msg_size )
	    local major_ver = buffer(3,1):uint()
	    local minor_ver = buffer(3,1):uint()
		local PreInitStatus = buffer(4,1):uint()
		minor_ver = minor_ver % 16 
		major_ver = ( major_ver - minor_ver ) /16
		hkeptree:add(buffer(2,1),"msg_id: " .. msg_id .. "  MassageType: AKE_PreInitStatus" )
		hkeptree:add(buffer(3,1),"Version: " .. buffer(3,1) .. "  MajorVersion: " .. major_ver .. ", MinorVersion: " .. minor_ver )
		if PreInitStatus == 0 then
			hkeptree:add(buffer(4,1),"status: " .. buffer(4,1) .." (Ok)")
		elseif PreInitStatus == 1 then
			hkeptree:add(buffer(4,1),"status: " .. buffer(4,1) .." (statusInvalidParameters)") 
		elseif PreInitStatus == 2 then
			hkeptree:add(buffer(4,1),"status: " .. buffer(4,1) .." (statusPairingExpired)")
		elseif PreInitStatus == 3 then
			hkeptree:add(buffer(4,1),"status: " .. buffer(4,1) .." (statusSessionExpired)")
		else
			hkeptree:add(buffer(4,1),"status: " .. buffer(4,1) .." (Reserved)")
		end
		hkeptree:add(buffer(5,2),"pairingSlots: " .. buffer(5,2):uint() )
		hkeptree:add(buffer(7,2),"sessionSlots: " .. buffer(7,2):uint() )
		hkeptree:add(buffer(9,16),"vendorExtension: " .. buffer(9,16) )
	end
end

-- パース用の関数定義
function hkep_proto.dissector(buffer,pinfo,tree)
	massagetype = ""
    local HEADDER_LENGTH = 2
--    if buffer:len() < HEADDER_LENGTH then
--        -- ヘッダ部に必要なデータ長が無いので、後続のパケットにアペンドする.
--        pinfo.desegment_len = DESEGMENT_ONE_MORE_SEGMENT
--        return buffer:len() - HEADDER_LENGTH
--    end

    while buffer:len() > 0 do
        local length = buffer(0,2):uint()
--        if (buffer:len() - HEADDER_LENGTH) < length then
--            -- データ部に必要なデータ長が無いので、後続のパケットにアペンドする.
--            pinfo.desegment_len = DESEGMENT_ONE_MORE_SEGMENT
--            return buffer:len() - length + HEADDER_LENGTH
--        end

        diso_hkep(buffer,pinfo,tree)
        if (buffer:len() - (HEADDER_LENGTH + length)) <= 0 then
			pinfo.cols.info = massagetype
            break
        end
        buffer = buffer:range(length)
    end
end

-- tcp.portテーブルのロード
tcp_table = DissectorTable.get("tcp.port")
-- ポート48879番とプロトコルの紐付けをする
tcp_table:add(12345,hkep_proto)


